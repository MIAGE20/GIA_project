package fr.hydraIt.gia.dto;

import lombok.Data;

@Data
public class AuthentificationDto {
    private String identifier;
    private String password;
}
