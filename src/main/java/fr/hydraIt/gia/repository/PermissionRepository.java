package fr.hydraIt.gia.repository;

import fr.hydraIt.gia.entity.PermissionInformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PermissionRepository extends JpaRepository<PermissionInformationEntity, Integer>{
}
