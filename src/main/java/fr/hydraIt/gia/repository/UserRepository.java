package fr.hydraIt.gia.repository;

import fr.hydraIt.gia.entity.UserInformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserInformationEntity, Integer>{
    Optional<UserInformationEntity> findByIdentifier(String identifier);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO user_profil (mskey, idpermissioninfo) VALUES (:idUser, :idPermission)", nativeQuery = true)
    void addPermission(@Param("idUser") int idUser, @Param("idPermission")int idPermission);


    @Modifying
    @Transactional
    @Query(value = "INSERT INTO user_working_information (mskey, idworkinginfo) VALUES (:mskey, :idworkinginfo)", nativeQuery = true)
    void addWorkingInfo(@Param("mskey") int idUser, @Param("idworkinginfo")int idWorkingInfo);

    @Modifying
    @Transactional
    @Query(value = "UPDATE  user_working_information SET idworkinginfo = :idworkinginfo WHERE mskey= :mskey", nativeQuery = true)
    void updateWorkingInfo(@Param("mskey") int idUser, @Param("idworkinginfo")int idWorkingInfo);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM  user_working_information WHERE mskey = :mskey", nativeQuery = true)
    void deleteUserWorkingInformation(@Param("mskey") int idUser);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM user_profil WHERE mskey =:mskey and idpermissioninfo =:idPermission", nativeQuery = true)
    void deleteUserPermission(@Param("mskey") int idUser, @Param("idPermission")int idPermission);
}
