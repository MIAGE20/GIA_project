package fr.hydraIt.gia.repository;

import fr.hydraIt.gia.entity.ApplicationInformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationRepository extends JpaRepository<ApplicationInformationEntity, Integer> {
}


