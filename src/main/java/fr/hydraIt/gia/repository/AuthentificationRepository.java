package fr.hydraIt.gia.repository;

import fr.hydraIt.gia.entity.AuthentificationInformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthentificationRepository extends JpaRepository<AuthentificationInformationEntity, Integer> {
    @Query("FROM AuthentificationInformationEntity WHERE idUser = ?1")
    Optional<AuthentificationInformationEntity> findByUserId(Integer userId);

}
