package fr.hydraIt.gia.repository;

import fr.hydraIt.gia.entity.WorkingInformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WorkingRepository extends JpaRepository<WorkingInformationEntity, Integer> {
    @Query("FROM WorkingInformationEntity WHERE idUser = ?1")
    Optional<WorkingInformationEntity> findByUserId(Integer userId);
}
