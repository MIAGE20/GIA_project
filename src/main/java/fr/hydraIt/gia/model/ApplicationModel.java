package fr.hydraIt.gia.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApplicationModel {
    private Integer idService;
    private String nomAppli;
}
