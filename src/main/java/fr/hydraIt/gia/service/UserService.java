package fr.hydraIt.gia.service;

import fr.hydraIt.gia.entity.UserInformationEntity;

public interface UserService {
    boolean validateConnection(String identifier, String mdp);
    Integer addUser(UserInformationEntity newUser);
    void addUserPermission(Integer idUser, Integer idPermission);
    void addUserWorkingInfo(Integer idUser, Integer idWorkingInfo);
    void resetPassword(Integer idUser, String newPWD);
    void deleteUserWorkingInfo(Integer idUser);
    void deleteUserPermission(Integer idUser, Integer idPermission);
}
