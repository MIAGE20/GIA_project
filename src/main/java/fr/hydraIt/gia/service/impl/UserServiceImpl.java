package fr.hydraIt.gia.service.impl;

import fr.hydraIt.gia.entity.AuthentificationInformationEntity;
import fr.hydraIt.gia.entity.UserInformationEntity;
import fr.hydraIt.gia.repository.AuthentificationRepository;
import fr.hydraIt.gia.repository.UserRepository;
import fr.hydraIt.gia.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Year;
import java.util.Optional;
import java.util.Random;


@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private AuthentificationRepository authentificationRepository;


    @Override
    public boolean validateConnection(String identifier, String mdp) {
        Optional<UserInformationEntity> user = userRepository.findByIdentifier(identifier);
        if (user.isPresent()) {
            AuthentificationInformationEntity wdp = user.get().getAuthent();
            if (wdp != null) {
                String m = wdp.getPassword();
                if (mdp.equals(m)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Integer addUser(UserInformationEntity newUser) {
        char fn = newUser.getFirstName().charAt(0);
        char ln = newUser.getLastName().charAt(newUser.getLastName().length() - 1);
        Year y = Year.now();
        int n = new Random().nextInt(99) + 1;
        String identifier = String.format("%c%c%04d%02d", fn, ln, y.getValue(), n).toUpperCase();
        newUser.setIdentifier(identifier);
        newUser.setDisplayname(newUser.getFirstName() + " " + newUser.getLastName());
        newUser.setIdUser(null);
        userRepository.save(newUser);
        setPassword(newUser.getIdUser());
        return newUser.getIdUser();
    }

    private String initialPassword() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            int mdp = new Random().nextInt(26);
            char c = (char) ('A' + mdp);
            str.append(c);
        }
        return str.toString();
    }

    private void setPassword(int idUser) {
        String pwd = initialPassword();
        AuthentificationInformationEntity entity = new AuthentificationInformationEntity();
        entity.setPassword(pwd);
        entity.setIdUser(idUser);
        entity.setPasswordInitial(true);
        authentificationRepository.save(entity);
    }

    @Override
    public void addUserPermission(Integer idUser, Integer idPermission) {
        userRepository.addPermission(idUser, idPermission);
    }

    @Override
    public void addUserWorkingInfo(Integer idUser, Integer idWorkingInfo) {
        if (userRepository.getOne(idUser).getWork() == null) {
            userRepository.addWorkingInfo(idUser, idWorkingInfo);
        } else {
            userRepository.updateWorkingInfo(idUser, idWorkingInfo);
        }
    }

    @Override
    public void resetPassword(Integer idUser, String newPWD) {
        UserInformationEntity user = userRepository.getOne(idUser);
        AuthentificationInformationEntity wdp = user.getAuthent();
        wdp.setPassword(newPWD);
        wdp.setPasswordInitial(false);
        authentificationRepository.save(wdp);
    }

    @Override
    public void deleteUserPermission(Integer idUser, Integer idPermission) {
        userRepository.deleteUserPermission(idUser, idPermission);
    }

    @Override
    public void deleteUserWorkingInfo(Integer idUser) {
        userRepository.deleteUserWorkingInformation(idUser);
    }
}