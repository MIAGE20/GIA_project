package fr.hydraIt.gia.resource;

import fr.hydraIt.gia.dto.AuthentificationDto;
import fr.hydraIt.gia.entity.UserInformationEntity;
import fr.hydraIt.gia.repository.UserRepository;
import fr.hydraIt.gia.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
@AllArgsConstructor
public class UserResource {
    private UserRepository userRepository;
    private UserService userService;

    @GetMapping(value = "/authentification")
    public Boolean validateConnection(@RequestBody AuthentificationDto dto) {
        return userService.validateConnection(dto.getIdentifier(), dto.getPassword());
    }

    @GetMapping(value = "/all")
    public List<UserInformationEntity> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public UserInformationEntity getUserById(@PathVariable Integer id) {
        return userRepository.findById(id).orElse(null);
    }

    @GetMapping(value = "/identifier/{identifier}")
    public UserInformationEntity getUserByIdentifier(@PathVariable String identifier) {
        return userRepository.findByIdentifier(identifier).orElse(null);
    }

    @PostMapping(value = "/add")
    public Integer addUser(@RequestBody final UserInformationEntity newUser) {
        return userService.addUser(newUser);
    }

    @PostMapping(value = "/update")
    public UserInformationEntity updateUser(@RequestBody final UserInformationEntity updatedUser) {
        return userRepository.save(updatedUser);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteUser(@PathVariable Integer id) {
        userRepository.deleteById(id);
    }

    @PostMapping(value = "/{idUser}/addUserPermission/{idPermission}")
    public void addUserPermission(@PathVariable Integer idUser, @PathVariable Integer idPermission) {
        userService.addUserPermission(idUser, idPermission);
    }

    @PostMapping(value = "/{idUser}/addUserWorkingInfo/{idWorkingInfo}")
    public void addUserWorkingInfo(@PathVariable Integer idUser, @PathVariable Integer idWorkingInfo) {
        userService.addUserWorkingInfo(idUser, idWorkingInfo);
    }

    @DeleteMapping(value = "/{idUser}/deleteUserPermission/{idPermission}")
    public void deleteUserPermission(@PathVariable Integer idUser, @PathVariable Integer idPermission) {
        userService.deleteUserPermission(idUser, idPermission);
    }

    @DeleteMapping(value = "/{idUser}/deleteUserWorkingInfo")
    public void deleteUserWorkingInfo(@PathVariable Integer idUser) {
        userService.deleteUserWorkingInfo(idUser);
    }

    @PostMapping(value = "/{idUser}/resetPWD")
    public void resetPWD(@PathVariable Integer idUser, @RequestBody String newPWD) {
        userService.resetPassword(idUser, newPWD);
    }
}
