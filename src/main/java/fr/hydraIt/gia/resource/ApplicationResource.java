package fr.hydraIt.gia.resource;
import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hydraIt.gia.entity.ApplicationInformationEntity;
import fr.hydraIt.gia.repository.ApplicationRepository;

@RestController
@RequestMapping(value ="/application")
@AllArgsConstructor
public class ApplicationResource {
	ApplicationRepository applicationRepository;
	
	@GetMapping(value = "/all")
	public List<ApplicationInformationEntity> getAllApplications(){
		return applicationRepository.findAll();
	}

	@GetMapping(value = "/{id}")
	public ApplicationInformationEntity getApplicationById(@PathVariable Integer id){
		return applicationRepository.findById(id).orElse(null);
	}
	
	@PostMapping(value = "/add")
	public ApplicationInformationEntity addApplication(@RequestBody final ApplicationInformationEntity application){
		return applicationRepository.save(application);
	}
	
	@PostMapping(value = "/update")
	public ApplicationInformationEntity updateApplication(@RequestBody final ApplicationInformationEntity newApplication) {
		return applicationRepository.save(newApplication);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void deleteApplication(@PathVariable Integer id){
		applicationRepository.deleteById(id);
	}

}
