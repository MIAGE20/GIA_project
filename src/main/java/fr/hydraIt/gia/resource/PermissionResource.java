
package fr.hydraIt.gia.resource;

import fr.hydraIt.gia.entity.PermissionInformationEntity;
import fr.hydraIt.gia.repository.PermissionRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/permission")
@AllArgsConstructor
public class PermissionResource {
	PermissionRepository permissionRepository;

	@GetMapping(value = "/all")
	public List<PermissionInformationEntity> getAllPermission() {
		return permissionRepository.findAll();
	}

	@GetMapping(value = "/{id}")
	public PermissionInformationEntity getPermissionById(@PathVariable Integer id) {
		return permissionRepository.findById(id).orElse(null);
	}

	@PostMapping(value = "/add")
	public PermissionInformationEntity addPermission(@RequestBody final PermissionInformationEntity permissionInformation) {
		return permissionRepository.save(permissionInformation);
	}

	@PostMapping(value = "/update")
	public PermissionInformationEntity updatePermission(@RequestBody final PermissionInformationEntity newPermissionInformation) {
		return permissionRepository.save(newPermissionInformation);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deletePermission(@PathVariable Integer id) {
		permissionRepository.deleteById(id);
	}

}
