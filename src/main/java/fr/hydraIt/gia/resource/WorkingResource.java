package fr.hydraIt.gia.resource;

import fr.hydraIt.gia.entity.WorkingInformationEntity;
import fr.hydraIt.gia.repository.WorkingRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/workingInfo")
@AllArgsConstructor
public class WorkingResource {
	WorkingRepository workingRepository;
	
	@GetMapping(value = "/all")
	public List<WorkingInformationEntity> getAllWorkingInfo() {
		return workingRepository.findAll();
	}

	@GetMapping(value = "/{id}")
	public WorkingInformationEntity getWorkingInfoById(@PathVariable Integer id) {
		return workingRepository.findById(id).orElse(null);
	}

	@PostMapping(value = "/add")
	public WorkingInformationEntity addWorkingInfo(@RequestBody final WorkingInformationEntity workingInfo) {
		return workingRepository.save(workingInfo);
	}

	@PostMapping(value = "/update")
	public WorkingInformationEntity updateWorkingInfo(@RequestBody final WorkingInformationEntity NewWorkingInfo) {
		return workingRepository.save(NewWorkingInfo);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deleteWorkingInfo(@PathVariable Integer id) {
		workingRepository.deleteById(id);
	}
}
