package fr.hydraIt.gia.resource;

import fr.hydraIt.gia.entity.AuthentificationInformationEntity;
import fr.hydraIt.gia.repository.AuthentificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value ="/authentification")
@AllArgsConstructor
public class AuthentificationResource {
	AuthentificationRepository authentificationRepository;
	
	@GetMapping(value = "/all")
	public List<AuthentificationInformationEntity> getAllAuthentifications(){
		return authentificationRepository.findAll();
	}

	@GetMapping(value = "/{id}")
	public AuthentificationInformationEntity getAuthentificationById(@PathVariable Integer id){
		return authentificationRepository.findById(id).orElse(null);
	}
	
	@PostMapping(value = "/add")
	public AuthentificationInformationEntity addAuthentification(@RequestBody final AuthentificationInformationEntity authentification){
		return authentificationRepository.save(authentification);
	}
	
	@PostMapping(value = "/update")
	public AuthentificationInformationEntity updateAuthentification(@RequestBody final AuthentificationInformationEntity NewAuthentification) {
		return authentificationRepository.save(NewAuthentification);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void deleteAuthentification(@PathVariable Integer id){
		authentificationRepository.deleteById(id);
	}

}
