package fr.hydraIt.gia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EnableJpaRepositories( basePackages = "fr.hydraIt.gia.repository")
@SpringBootApplication
public class GiaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiaProjectApplication.class, args);
	}

}
