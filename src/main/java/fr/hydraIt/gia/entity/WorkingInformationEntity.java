package fr.hydraIt.gia.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "working_information")
@Entity
@Data
public class WorkingInformationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idworkinginfo")
    private Integer idWorkingInfo;

    @Column(name = "hd_company")
    private String company;

    @Column(name = "hd_costcenter")
    private String costcenter;

    @Column(name = "hd_organizationunit")
    private String organizationUnit;

    @Column(name = "hd_businessunit")
    private String businessUnit;

    @Column(name = "hd_office_addr1")
    private String officeAddr1;

    @Column(name = "hd_office_addr2")
    private String officeAddr2;

    @Column(name = "hd_office_postal")
    private String officePostal;

    @Column(name = "hd_office_city")
    private String officeCity;

    @Column(name = "hd_office_country")
    private String officeCountry;
}
