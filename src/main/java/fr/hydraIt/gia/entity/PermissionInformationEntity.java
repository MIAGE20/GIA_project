package fr.hydraIt.gia.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "permission_information")
@Data
public class PermissionInformationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpermissioninfo")
    private Integer idPermissionInfo;

    @Column(name = "hd_permission")
    private String permissionName;

}
