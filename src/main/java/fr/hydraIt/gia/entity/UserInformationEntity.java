package fr.hydraIt.gia.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Table(name = "user_information")
@Entity
@Data
public class UserInformationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mskey")
    private Integer idUser;

    @Column(name = "mskeyvalue")
    private String identifier;

    @Column(name = "displayname")
    private String displayname;

    @Column(name = "hd_firstname")
    private String firstName;

    @Column(name = "hd_lastname")
    private String lastName;

    @Column(name = "hd_birthday")
    private String birthday;

    @Column(name = "hd_salutation")
    private String salutation;

    @Column(name = "hd_phone")
    private String phone;

    @Column(name = "hd_mobile")
    private String mobile;

    @Column(name = "hd_fax")
    private String fax;

    @Column(name = "hd_language")
    private String language;

    @Column(name = "hd_email")
    private String email;

    @Column(name = "hd_country")
    private String country;

    @Column(name = "hd_address1")
    private String address1;

    @Column(name = "hd_address2")
    private String address2;

    @Column(name = "hd_postal_code")
    private String postalCode;

    @Column(name = "hd_city")
    private String city;


    @OneToMany
    @JoinTable(
            name = "user_profil",
            joinColumns = @JoinColumn(name = "mskey"),
            inverseJoinColumns = @JoinColumn(name = "idpermissioninfo")
    )
    private Set<PermissionInformationEntity> permissions;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "user_working_information",
            joinColumns = {@JoinColumn(name = "mskey", referencedColumnName = "mskey")},
            inverseJoinColumns = {@JoinColumn(name = "idworkinginfo", referencedColumnName = "idworkinginfo")})
    private WorkingInformationEntity work;

    // See: https://www.baeldung.com/jackson-bidirectional-relationships-and-infinite-recursion
    @OneToOne
    @JoinTable(name = "authentification_information",
            joinColumns = {@JoinColumn(name = "mskey", referencedColumnName = "mskey")},
            inverseJoinColumns = {@JoinColumn(name = "idauthentificationinfo", referencedColumnName = "idauthentificationinfo")})
    private AuthentificationInformationEntity authent;


    // Suppression de la validation du schema par hibernate
    // See: https://stackoverflow.com/questions/47605790/hibernate-unable-to-add-foreign-key-constraint
    @OneToMany
    @JoinTable(
            name = "user_authorized_service",
            joinColumns = @JoinColumn(name = "mskey"),
            inverseJoinColumns = @JoinColumn(name = "idservice")
    )
    private Set<ApplicationInformationEntity> applis;


}