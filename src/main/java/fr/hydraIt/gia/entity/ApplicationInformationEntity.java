package fr.hydraIt.gia.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "application_information")
@Data
public class ApplicationInformationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "idservice")
    private Integer idService;

    @Column(name = "nomappli")
    private String nomAppli;
}
