package fr.hydraIt.gia.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name = "authentification_information")
@Entity
@Data
public class AuthentificationInformationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "idauthentificationinfo")
    private Integer idAuthentificationInfo;

    @Column(name = "hd_password")
    private String password;

    @Column(name = "hd_password_initial")
    private boolean passwordInitial;

    @Column(name = "hd_password_locked")
    private boolean passwordLocked;

    @Column(name = "hd_disabled")
    private boolean passwordDisabled;

    @Column(name = "mskey")
    private Integer idUser;

}
