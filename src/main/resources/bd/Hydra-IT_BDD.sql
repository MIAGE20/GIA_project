/* creation du user */
DROP USER IF EXISTS 'ahlem'@'localhost';
CREATE USER 'ahlem'@'localhost' IDENTIFIED BY 'miage';
GRANT ALL PRIVILEGES ON *.* TO 'ahlem'@'localhost';

/* creation de la BDD */
drop database if exists HYDRA;
create database HYDRA;
use HYDRA;

create table user_information (
    mskey int not null auto_increment,
    mskeyvalue varchar(100) unique , 
    displayname varchar(100), 
    hd_firstname varchar(100),
    hd_lastname varchar(100),
    hd_birthday date,
    hd_salutation varchar(100),
    hd_email varchar(100) unique,
    hd_country varchar(100),
    hd_address1 varchar(100),
    hd_address2 varchar(100),
    hd_postal_code varchar(100),
    hd_city varchar(100),
    hd_phone varchar(100),
    hd_mobile varchar(100),
    hd_fax varchar(100),
    hd_language varchar(100),
    primary key (MSKEY)
)  ENGINE=INNODB;

create table working_information (
    idworkinginfo int AUTO_INCREMENT,
    hd_company varchar(100),
    hd_costcenter varchar(100),
    hd_organizationunit varchar(100),  -- security governence --
    hd_businessunit varchar(100),
    hd_office_addr1 varchar(100),
    hd_office_addr2 varchar(100),
    hd_office_postal varchar(100),
    hd_office_city varchar(100),
    hd_office_country varchar(100),
    primary key (idworkinginfo)
) ENGINE=INNODB;

create table authentification_information
 (
    idauthentificationinfo int AUTO_INCREMENT,
    hd_password varchar(100),
    hd_password_initial Boolean,
    hd_password_locked Boolean,
    hd_disabled Boolean,
    mskey int,
    primary key (idauthentificationinfo),
    foreign key (mskey) references user_information(mskey) ON DELETE CASCADE
) ENGINE=INNODB;

create table permission_information (
    idpermissioninfo int AUTO_INCREMENT,
    hd_permission varchar(100) unique,
    primary key (idpermissioninfo)
) ENGINE=INNODB;

-- creation de la table Services pour le test
create table application_information (
    idservice int AUTO_INCREMENT,
    nomappli varchar(250) unique,
    primary key (idservice)
) ENGINE=INNODB;

create table user_working_information (
    mskey int,
    idworkinginfo int,
    primary key (mskey, idworkinginfo),
    unique (mskey),
    FOREIGN KEY (mskey) references user_information(mskey) ON DELETE CASCADE,
    FOREIGN KEY (idworkinginfo) references working_information(idworkinginfo) ON DELETE CASCADE
) ENGINE=INNODB;

create table user_profil (
    mskey int,
    idpermissioninfo int,
    primary key (mskey, idpermissioninfo),
    FOREIGN KEY (mskey) references user_information(mskey) ON DELETE CASCADE,
    FOREIGN KEY (idpermissioninfo) references permission_information(idpermissioninfo) ON DELETE CASCADE

) ENGINE=INNODB;

create table profil_authorized (
    idservice int not null ,
    idpermissioninfo int not null,
    primary key (idservice, idpermissioninfo),
    FOREIGN KEY (idservice) references application_information(idservice) ON DELETE CASCADE,
    FOREIGN KEY (idpermissioninfo) references permission_information(idpermissioninfo) ON DELETE CASCADE
) ENGINE=INNODB;

/* Creation des views */
/* Recupere les applications des users : */

CREATE VIEW user_authorized_service AS
SELECT DISTINCT u.mskey, a.*
FROM application_information a
JOIN profil_authorized p ON p.idservice = a.idservice
JOIN user_profil u ON u.idpermissioninfo = p.idpermissioninfo;


/* Insertion des données pour le test */

insert into user_information values (1, "FA052001", 'FANTAZI AHLEM', "Ahlem", "FANTAZI", "1988-09-15", "Madame",
                                        "afanta@yahoo.fr", "Belgique", "8 rue de bruxelles"," residence de sevres" ,"1060", "Bruxelles",
                                        "0195010203", "0695010203", "0270709664", "français"),

                                    (2, "AM041701", 'AIDOUDI Mohamed', "Mohamed", "AIDOUDI", "1990-08-29", "Monsieur",
                                        "maidoudi@gmail.com", "France", "10 rue hydra","residence de service","75000", "Paris",
                                        "0294040506", "0694040506", "0270709665", "français"),


                                    (3, "SY061901", 'SHI Yanjie', "Yanjie", "SHI", "1988-04-30", "Madame",
                                        "yshi@live.fr", "Chine", "9 rue des pommiers"," residence de roys" ,"100000", "Pekin",
                                        "0393070809", "0693070809", "0270709666", "anglais");



insert into working_information values (20, "Paris 1", "centre des couts 1", "unite d'organisation 1" ,"unite d'affaire 1" , " adresse du bureau 1", "adresse du bureau bis 1","7500", "Paris", "FRANCE"),
                                       (21, "Hydra-IT","centre des couts 2", "unite d'organisations 2" ,"unite d'affaire 2", "adresse du bureau 2 ", "adresse du bureau bis 2", "7500", "Paris", "FRANCE"),
                                       (22, "Paris 1", "centre des couts 3", "unite d'organisation 3" , "unite d'affaire 3","adresse du bureau 3 ", "adresse du bureau bis 3","7500", "Paris", "FRANCE");

insert into authentification_information values (31, "hydra123", 0, 0, 0, 1),
                                                (32, "hydra456", 0, 0, 0, 2),
                                                (33, "hydra678", 0, 0, 0, 3);

insert into permission_information values (41, "S4H_SAP_ALL"),
                                          (42, "DPO_SUPERADMIN"),
                                          (43, "S4H_SAP_NEW"),
                                          (44, "PPF_PIMON"),
                                          (45, "PPF_UME_ADMIN");

insert into application_information values (51, "SAP ERP"),
                                           (52, "jira"),
                                           (53, "SAP HANA"),
                                           (54, "mail");

insert into user_profil values (1, 41);
insert into user_profil values (2, 42);
insert into user_profil values (3, 43);


insert into user_working_information values (1, 20);
insert into user_working_information values (2, 21);
insert into user_working_information values (3, 22);


insert into profil_authorized values (51, 41), 
                                     (54, 41), 
                                     (52, 42), 
                                     (53, 43);
                                  

/* requetes 
-- afficher la personne + type d'acces + service autorisé --
select u.hd_lastname, per.hd_permission, ap.nomappli 
from user_information u, user_profil p, permission_information per, profil_authorized pro, application_information ap
where 1=1
AND u.MSKEY = p.MSKEY
and p.idpermissioninfo = per.idpermissioninfo
and pro.idpermissioninfo = per.idpermissioninfo
and pro.idservice = ap.idservice;

select u.hd_lastName, per.hd_permission, ap.nomappli 
        from user_information u
        join user_profil p on u.MSKEY = p.MSKEY
        join permission_information per on p.idpermissioninfo = per.idpermissioninfo
        join profil_authorized pro on pro.idpermissioninfo = per.idpermissioninfo
        join application_information ap on ap.idservice = pro.idservice
        where 1=1 ;
 
+-------------+---------------+-----------+
| hd_lastName | hd_permission | nomAppli  |
+-------------+---------------+-----------+
| FANTAZI     | S4H_SAP_AL    | jira      |
| AIDOUDI     | DPO_SUPERA    | adminUser |
| SHI         | PPF_UME_AD    | mail      |
+-------------+---------------+-----------+
3 rows in set (0.03 sec)
*/