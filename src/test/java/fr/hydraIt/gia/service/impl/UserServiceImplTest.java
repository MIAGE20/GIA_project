package fr.hydraIt.gia.service.impl;

import fr.hydraIt.gia.entity.AuthentificationInformationEntity;
import fr.hydraIt.gia.entity.UserInformationEntity;
import fr.hydraIt.gia.repository.AuthentificationRepository;
import fr.hydraIt.gia.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Optional;
import java.util.Random;

// See: https://mkyong.com/spring-boot/spring-boot-junit-5-mockito/
class UserServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private AuthentificationRepository authentificationRepository;

    @InjectMocks
    private UserServiceImpl userServiceToTest;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Nested
    class ConnectionValidationTest {
        @Test
        void validateConnection_should_return_false_when_user_does_not_exist() {
            // Given:
            Mockito.when(userRepository.findByIdentifier("ahlem")).thenReturn(Optional.empty());
            // When:
            boolean actual = userServiceToTest.validateConnection("ahlem", "myPass");
            // Then:
            Assertions.assertThat(actual).isFalse();
        }

        @Test
        void validateConnection_should_return_false_when_user_password_is_invalid() {
            // Given:
            UserInformationEntity user = new UserInformationEntity();
            user.setAuthent(new AuthentificationInformationEntity());
            user.getAuthent().setPassword("s0m3P@ssw0rd");

            Mockito.when(userRepository.findByIdentifier("ahlem")).thenReturn(Optional.of(user));
            // When:
            boolean actual = userServiceToTest.validateConnection("ahlem", "myPassword");
            // Then:
            Assertions.assertThat(actual).isFalse();
        }

        @Test
        void validateConnection_should_return_true_when_user_password_is_valid() {
            // Given:
            UserInformationEntity user = new UserInformationEntity();
            user.setAuthent(new AuthentificationInformationEntity());
            user.getAuthent().setPassword("s0m3P@ssw0rd");

            Mockito.when(userRepository.findByIdentifier("ahlem")).thenReturn(Optional.of(user));
            // When:
            boolean actual = userServiceToTest.validateConnection("ahlem", "s0m3P@ssw0rd");
            // Then:
            Assertions.assertThat(actual).isTrue();
        }
    }

    @Nested
    class UserPasswordTest {
        private UserInformationEntity createNewUser() {
            UserInformationEntity user = new UserInformationEntity();
            user.setFirstName("Jane");
            user.setLastName("Doe");

            return user;
        }

        @Test
        void newUser_should_be_created_with_generated_password() {
            // Given:
            int newUserId = new Random().nextInt();
            UserInformationEntity newUser = createNewUser();
            Mockito.doAnswer(invocation -> {
                newUser.setIdUser(newUserId);
                return newUser;
            }).when(userRepository).save(newUser);

            // When:
            userServiceToTest.addUser(newUser);

            // Then:
            ArgumentCaptor<AuthentificationInformationEntity> capturedAuthent = ArgumentCaptor.forClass(AuthentificationInformationEntity.class);
            Mockito.verify(authentificationRepository).save(capturedAuthent.capture());

            AuthentificationInformationEntity authent = capturedAuthent.getValue();
            Assertions.assertThat(authent.getIdUser()).as("idUser").isEqualTo(newUserId);
            Assertions.assertThat(authent.getPassword()).as("password").isNotEmpty();
            Assertions.assertThat(authent.isPasswordInitial()).as("isInitialPassword").isTrue();
        }

        @Test
        void resetPassword_should_clear_passInitialFlag() {
            // Given:
            UserInformationEntity user = new UserInformationEntity();
            user.setIdUser(123);
            user.setAuthent(new AuthentificationInformationEntity());
            user.getAuthent().setPasswordInitial(true);
            user.getAuthent().setPassword("changeme");

            Mockito.when(userRepository.getOne(123)).thenReturn(user);

            // When:
            userServiceToTest.resetPassword(123, "newPassword");

            // Then:
            Assertions.assertThat(user.getAuthent().getPassword()).isEqualTo("newPassword");
            Assertions.assertThat(user.getAuthent().isPasswordInitial()).isFalse();
            Mockito.verify(authentificationRepository).save(user.getAuthent());
        }
    }
}